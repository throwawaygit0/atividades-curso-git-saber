# GERAL

Bem vindos ao curso de git!

Abaixo estão instruções do que deve ser realizado em cada uma das atividades.

Vocês podem escolher uma das três atividades para fazer. Se sobrar tempo, podem fazer as outras também!

As equipes devem ser compostas de **NO MÁXIMO** 2 membros.

Um erro comum é não entender o que os conflitos causados pelo `git merge` significam. Abaixo está um exemplo explicando o que acontece:

```c
void funcao () {
    int variavel;
   
<<<<<<< HEAD
    //codigo que esta aqui agora
=======
    //codigo que veio do merge
>>>>>>> lugardeondeveio

    return;
}
```

# NOTA-IMPORTANTE-PACAS

Criem uma branch principal, chamada `develop`, a partir da `master`. É lá que vocês devem realizar suas alterações!

A branch `master` deve ser atualizada **SOMENTE** após as atividades estarem completas e estáveis!

## Atividade em C

Vocês terão duas atividades principais, que estão descritas abaixo:

- Uma delas está na pasta `C/`, em que a atividade é a seguinte:
    - **Descrição:** Nesta pasta, um programa que copia o arquivo `data.txt` para o arquivo `data_copy.txt` foi feito, mas está incompleto. Seu objetivo é consertar o programa, seguindo os passos a seguir:
        - Um dos membros deve criar uma branch chamada `fix-escritor`, a partir da `develop`, em que deve arrumar os erros contidos no arquivo `escritor.c` **APENAS!**
        - O outro membro deve criar uma branch chamada `fix-leitor`, a partir da `develop`, em que, da mesma forma, deve arrumar os erros contidos no arquivo `leitor.c`
        - Ao final, ambos devem dar `git merge` de suas *branches* na `develop`
        - Em seguida, uma nova branch chamada `test-develop` deve ser criada, a partir da nova branch `develop` atualizada
        - Os dois membros devem entrar na branch `test-develop`, abrir um terminal na pasta e rodar o comando `make`, e então tentar corrigir os erros remanescentes.
        - **NOTA:** Ao rodar o comando `make`, se não ocorrerem erros de compilação, um arquivo executável chamado `copiaarquivotexto.exe` é criado. Você precisa executar este programa com `./copiaarquivotexto.exe` para verificar se está tudo funcionando!

- A outra atividade está na pasta `C_Fatorial/`, em que a atividade é a seguinte:
    - **Descrição**: Nesta pasta, um programa simples, contido no arquivo `main.c`, está quase completo. Você só precisa implementar a função `fatorial()`, seguindo os passos a seguir:
        - Um dos membros deve implementar a função `fatorial()`, em uma nova branch chamada `fatorial-for`, utilizando um laço do tipo `for()`
        - O outro membro deve implementar a função `fatorial()`, em uma nova branch chamada `fatorial-while`, utilizando um laço do tipo `while()`
        - Ao final, ambos devem atualizar suas branches e dar `git merge` na `develop`. Desta vez, é recomendado que façam isso juntos, em um PC só.

**Ao encerrar ambas as atividades, chamem algum de nós para que façamos a avaliação e decidir se a atividade foi completada ou não. Não faça o `git merge` na `master` antes disso!**


## Atividade em Pascal

Vocês terão duas atividades principais, que estão descritas abaixo:

- Uma delas está na pasta `Pascal/`, em que a atividade é a seguinte:
    - **Descrição:** Nesta pasta, um programa que copia o arquivo `data.txt` para o arquivo `data_copy.txt` foi feito, mas está incompleto. Seu objetivo é consertar o programa, seguindo os passos a seguir:
        - Um dos membros deve criar uma branch chamada `fix-escritor`, a partir da `develop`, em que deve arrumar os erros contidos no arquivo `escritor.c` **APENAS!**
        - O outro membro deve criar uma branch chamada `fix-leitor`, a partir da `develop`, em que, da mesma forma, deve arrumar os erros contidos no arquivo `leitor.c`
        - Ao final, ambos devem dar `git merge` de suas *branches* na `develop`
        - Em seguida, uma nova branch chamada `test-develop` deve ser criada, a partir da nova branch `develop` atualizada
        - Os dois membros devem entrar na branch `test-develop`, abrir um terminal na pasta e rodar o comando `make`, e então tentar corrigir os erros remanescentes.
        - **NOTA:** Ao rodar o comando `make`, se não ocorrerem erros de compilação, um arquivo executável chamado `copiaarquivotexto.exe` é criado. Você precisa executar este programa com `./copiaarquivotexto.exe` para verificar se está tudo funcionando!

- A outra atividade está na pasta `Pascal_Fatorial/`, em que a atividade é a seguinte:
    - **Descrição**: Nesta pasta, um programa simples, contido no arquivo `main.c`, está quase completo. Você só precisa implementar a função `fatorial()`, seguindo os passos a seguir:
        - Um dos membros deve implementar a função `fatorial()`, em uma nova branch chamada `fatorial-for`, utilizando um laço do tipo `for()`
        - O outro membro deve implementar a função `fatorial()`, em uma nova branch chamada `fatorial-while`, utilizando um laço do tipo `while()`
        - Ao final, ambos devem atualizar suas branches e dar `git merge` na `develop`. Desta vez, é recomendado que façam isso juntos, em um PC só.

**Ao encerrar ambas as atividades, chamem algum de nós para que façamos a avaliação e decidir se a atividade foi completada ou não. Não faça o `git merge` na `master` antes disso!**

## Atividade em LaTeX

Vocês terão uma atividade, que deve ser feita em duas partes, **uma por membro**:

- Um membro da equipe deve criar uma branch chamada `reorder-chapters`, a partir da branch `develop`, e nela deve verificar se os capítulos estão corretos
- O outro membro fica deve criar uma branch chamada `insert-chapter-4`, a partir da branch `develop`, e nela, ele ficará encarregado de adicionar o capítulo IV do livro utilizando o comando input, conforme presente no arquivo `tex/meu_livro.tex`. O arquivo do input **DEVE** se chamar `chapter-4`!
- Ao terminar as atividades, ambos devem atualizar suas respectivas branches, e ao final devem se juntar em um único PC e dar `git merge` de suas atividades na branch `develop`, consertando os erros que ocorrerem.

**NOTA:** Mais informações estão no arquivo .tex da atividade. Você pode gerar o arquivo .pdf rapidamente abrindo um terminal na pasta da atividade e rodando o comando `make`!

**Ao encerrar ambas as atividades, chamem algum de nós para que façamos a avaliação e decidir se a atividade foi completada ou não. Não faça o `git merge` na `master` antes disso!**